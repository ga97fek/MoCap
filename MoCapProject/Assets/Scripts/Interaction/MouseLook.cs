﻿using UnityEngine;

[AddComponentMenu("Camera-Control/Mouse Look")]
public class MouseLook : MonoBehaviour
{
    public GameObject targetPlayer;
    public float cameraDistance= 15f;
    public float mouseScrollSpeed = 5f;

    public float mousesensitivityX = 15F;
    public float mousesensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    float rotationY = 0F;



    void Update()
    {
        float mouseWheelInput = Input.GetAxis("Mouse ScrollWheel");
        cameraDistance -= mouseWheelInput * mouseScrollSpeed;

        if (Input.GetMouseButton(1))
        {
            float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * mousesensitivityX;

            rotationY += Input.GetAxis("Mouse Y") * mousesensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }

        this.transform.position = targetPlayer.transform.position;
        this.transform.Translate(Vector3.forward * -cameraDistance, Space.Self);
        this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles.x, this.transform.rotation.eulerAngles.y, 0f);
    }

    void Start()
    {
        // Make the rigid body not change rotation
        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().freezeRotation = true;
    }
}