﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeanValueDeformation : MonoBehaviour {

    private struct RawVec4
    {
        public float x;
        public float y;
        public float z;
        public float w;
    }

#pragma warning disable 0649 // "field is never assigned to" warning
    [Header("Press Space to Update Mesh")]
    [SerializeField]
    private MeshFilter ownMeshRef;
    [SerializeField]
    private MeshFilter cageMeshRef;
    [SerializeField]
    private GameObject cagePointPrefab;

    [SerializeField]
    [Range(1, 4)]
    private float scaleMesh = 1f;

    [SerializeField]
    private bool ComputeOnGPU;
    [SerializeField]
    private ComputeShader VertexUpdateShader;

    [SerializeField]
    [Range(0, 30)]
	[Tooltip("value x => precision = 10^-x \nspecial case: 0 => max precision")]
    private int precisionInDecimals;

    [SerializeField]
    private bool CreateSpringJoints = false;

    [SerializeField]
    private int SpringCageMaxSize = 50;
#pragma warning restore 0649

    private Transform[] cagePoints;
    private int[] triangles;

    /// <summary>
    /// 2D weights array. Dimensions should be: [ownMesh.mesh.vertexCount, cagePoints.Length] |/|
    /// cageWeights.Lenght returns total number of weights (= own vertices * cage points) |/|
    /// cageWeights.GetLength(0) gives x length (= num of own vertices) |/| 
    /// cageWeights.GetLength(1) gives y length (= cagePoints.Length)
    /// </summary>
    private float[,] cageWeights;

    private float precision;

    // colorization variables
    private int cageVertexTarget = 0;
    [SerializeField]
    [Range(0.0f, 30.0f)]
    private float colorIntensity = 10f;
    private float lastColorIntensity = 0f;

    // interaction
    private MouseClick clickInfo;
    private bool alwaysUpdate = false;

    // compute shader vars
    private int kernelID;
    private ComputeBuffer weightsBuffer;

    // Use this for initialization
    void Start()
    {
        clickInfo = FindObjectOfType<MouseClick>();
        precision = precisionInDecimals == 0 ? 0 : Mathf.Pow(10, -precisionInDecimals);

        // convert mesh to cagePoints and triangles
        convertMeshToCagePoints(cageMeshRef.sharedMesh, cageMeshRef.transform);

        // calculate weights of cagePoints for every meshPoint
        calculateWeights();

        // delete mesh
        cageMeshRef.gameObject.GetComponent<MeshRenderer>().enabled = false;

        colorateMesh();

        // computeshader setup
        kernelID = VertexUpdateShader.FindKernel("CSVerticesFromCage");
        VertexUpdateShader.SetInt("numOfCagePoints", cagePoints.Length);
        setWeightsBuffer();
    }

    // Update is called once per frame
    void Update()
    {
        // update meshPoints based on cagePoints
        if (alwaysUpdate || Input.GetKeyDown(KeyCode.Space))
        {
            updateMesh();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            alwaysUpdate = !alwaysUpdate;
        }

        // select clicked cagepoint
        for (int i = 0; i < cagePoints.Length; i++)
        {
            if (clickInfo.clickedObject &&
                i != cageVertexTarget &&
                clickInfo.clickedObject.gameObject.GetInstanceID() ==
                cagePoints[i].gameObject.GetInstanceID())
            {
                cageVertexTarget = i;
                colorateMesh();
            }    
        }

        if (lastColorIntensity != colorIntensity)
        {
            colorateMesh();
            lastColorIntensity = colorIntensity;
        }
    }

    /// <summary>
    /// Spawns objects at each inputMesh vertex and fills them into the cagePoints array.
    /// </summary>
    /// <param name="inputMesh">Mesh we want to use as cage.</param>
    /// <param name="cageTransform">Transform on the inputMesh.</param>
    void convertMeshToCagePoints(Mesh inputMesh, Transform cageTransform)
    {
        Transform cageParent = new GameObject(cageMeshRef.gameObject.name + "-Container").transform;
        cageParent.SetPositionAndRotation(cageMeshRef.gameObject.transform.position, cageMeshRef.gameObject.transform.rotation);

        cagePoints = new Transform[inputMesh.vertexCount];
        for (int currVertex = 0; currVertex < cagePoints.Length; currVertex++)
        {
            Vector3 vertexPos = cageTransform.TransformPoint(inputMesh.vertices[currVertex]);
            cagePoints[currVertex] = Instantiate(cagePointPrefab, vertexPos, Quaternion.identity).transform;
            cagePoints[currVertex].transform.parent = cageParent;
        }
        triangles = inputMesh.triangles;


        if(cagePoints.Length > SpringCageMaxSize)
        {
            CreateSpringJoints = false;
        }
        if(CreateSpringJoints)
        {
            alwaysUpdate = true;

            GameObject[] cpobj = new GameObject[cagePoints.Length];
            for(int i = 0; i < cagePoints.Length; i++)
            {
                cpobj[i] = cagePoints[i].gameObject;
                cpobj[i].AddComponent<Rigidbody>();
            }

            for(int i = 0; i < cagePoints.Length; i++)
                for(int j = i + 1; j < cagePoints.Length; j++)
                {
                    SpringJoint s = cpobj[i].AddComponent<SpringJoint>();
                    s.connectedBody = cpobj[j].GetComponent<Rigidbody>();
                }
        }
    }
 

    #region weight calculation

    void calculateWeights()
    {
        string db = "";

        int numOfMeshPoints = ownMeshRef.mesh.vertexCount;
        int numOfCagePoints = cageMeshRef.mesh.vertexCount;
        int numOfTriangles = cageMeshRef.mesh.triangles.Length / 3;

        db += ("CW on mesh[" + numOfMeshPoints + "], cage[" + numOfCagePoints + "] with " + numOfTriangles + " triangles. Weights shown with 5 decimal places.\n");

        // durr 0.o these need to be per mesh vertex
        // so the weigths for each vertex add up to 1
        // instead of to something ridiculously small.
        float[] totalWeights = new float[numOfMeshPoints];

        // array for saving weights
        float[,] weights = new float[numOfMeshPoints, numOfCagePoints];

        // get cagepoints in world space
        Vector3[] worldCage = new Vector3[numOfCagePoints];
        for (int i = 0; i < numOfCagePoints; i++)
        {
            worldCage[i] = cagePoints[i].position;
        }

        // for each mesh vertex:
        for (int vertexIndex = 0; vertexIndex < numOfMeshPoints; vertexIndex++)
        {
            // get vertex in world space
            Vector3 worldSelf = transform.TransformPoint(ownMeshRef.mesh.vertices[vertexIndex]);

            // normalize cage points
            bool flagCagePointAtWorldSelf = false;
            float[] cagePointDistances = new float[numOfCagePoints];
            Vector3[] normalizedCage = new Vector3[numOfCagePoints];
            //db = "nc:";
            for (int i = 0; i < numOfCagePoints; i++)
            {
                Vector3 v = (worldCage[i] - worldSelf);
                cagePointDistances[i] = v.magnitude;
                normalizedCage[i] = v.normalized;
                //db += " | " + i + ":" + normalizedCage[i].ToString();

                // handle cases where some wc[i] are at worldSelf
                if (cagePointDistances[i] <= precision)
                {
                    flagCagePointAtWorldSelf = true;
                    // set weight of those wc[i] to 1 
                    // (they'll still be normalized later)
                    // All other weights remain at 0
                    weights[vertexIndex,i] = 1f;
                    totalWeights[vertexIndex] += 1f;
                }
            }
            // skip per triangle calculation if weights already have been determined
            if(flagCagePointAtWorldSelf)
                continue;

            // from this point on the current vertex is assumed to be at (0,0,0)
            // also there are no cage points directly at the current vertex (worldSelf)

            // calculate weights per triangle
            for (int triangleIndex = 0; triangleIndex < numOfTriangles; triangleIndex++)
            {
                int p1Index = cageMeshRef.mesh.triangles[(triangleIndex * 3) + 0];
                int p2Index = cageMeshRef.mesh.triangles[(triangleIndex * 3) + 1];
                int p3Index = cageMeshRef.mesh.triangles[(triangleIndex * 3) + 2];

                Vector3 p1Pos = normalizedCage[p1Index];
                Vector3 p2Pos = normalizedCage[p2Index];
                Vector3 p3Pos = normalizedCage[p3Index];

                float p1Dist = cagePointDistances[p1Index];
                float p2Dist = cagePointDistances[p2Index];
                float p3Dist = cagePointDistances[p3Index];

                float w1, w2, w3;

                if(calculateTriangleWeights(
                    p1Pos, p2Pos, p3Pos, 
                    p1Dist, p2Dist, p3Dist,
                    out w1, out w2, out w3))
                {
                    // returned weights are exclusive
                    // cause current vertex is inside the triangle
                    // -> overwrite previously calculated weights
                    for (int i = 0; i < cagePoints.Length; i++)
                    {
                        weights[vertexIndex, i] = 0; 
                    }
                    weights[vertexIndex,p1Index] = w1;
                    weights[vertexIndex,p2Index] = w2;
                    weights[vertexIndex,p3Index] = w3;

                    totalWeights[vertexIndex] = w1 + w2 + w3;

                    // skip calculation of the remaining triangles
                    break;
                }

                // add weight to point
                weights[vertexIndex,p1Index] += w1;
                weights[vertexIndex,p2Index] += w2;
                weights[vertexIndex,p3Index] += w3;

                // add weight to total weights 
                totalWeights[vertexIndex] += w1 + w2 + w3;
            }//loop: triangles

        }//loop: mesh vertices

        // normalize weights per point
        for (int i = 0; i < numOfMeshPoints; i++)
        {
            totalWeights[i] = 1.0f / totalWeights[i]; 

            float w = 0;
            // db += "P: " + i;
            for (int j = 0; j < numOfCagePoints; j++)
            {
                weights[i,j] *= totalWeights[i];
                db += " | " + weights[i,j].ToString("0.00000");
                w += weights[i,j];
            }
            // db += " | -> Sum: " + w.ToString() +"\n";
        }
        cageWeights = weights;
        Debug.Log(db);
    }

    /// <summary>
    /// calculates partial weights resulting from a single triangle
    /// </summary>
    /// <param name="p1Pos">normalized position of point 1 in the triangle</param>
    /// <param name="p2Pos">normalized position of point 2 in the triangle</param>
    /// <param name="p3Pos">normalized position of point 3 in the triangle</param>
    /// <param name="p1Dist">distance from point 1 to the current mesh vertex</param>
    /// <param name="p2Dist">distance from point 2 to the current mesh vertex</param>
    /// <param name="p3Dist">distance from point 3 to the current mesh vertex</param>
    /// <param name="weight1">resulting weight for point 1 from the current triangle</param>
    /// <param name="weight2">resulting weight for point 2 from the current triangle</param>
    /// <param name="weight3">resulting weight for point 3 from the current triangle</param>
    /// <returns>
    /// true if the returned weights should be regarded as exclusive weights for the current vertex.
    /// false if everything is normal.
    /// </returns>
    private bool calculateTriangleWeights(
        Vector3 p1Pos, Vector3 p2Pos, Vector3 p3Pos, 
        float p1Dist, float p2Dist, float p3Dist,
        out float weight1, out float weight2, out float weight3)
    {
        weight1 = weight2 = weight3 = 0;
        // reminder: current vertex v implicitly is at (0,0,0)

        // calc length of sphere triangle edges
        // (= angle between the vectors from v to the edge endpoints in radians)
        /** example calculation for edge3 between p1 and p2
         * +y    p1   _
         * |    /|    |      dist1 = sin(a) = |p1 - p2| /2
         * |   / |    |      => a) = arcsin(dist1)
         * |  /  |   dist1
         * | /   |    |      edge3 = 2 * a)  (angle with v at 0)
         * |/a)  |    |                      (p1 & p2 on unit circle)
         * 0-------------1-->+x
         * |\    |       |
         * | \   |      |
         * |  \  |     / ----somewhat wonky half of the
         * |   \ |    /      circular arc "edge3" that
         * |    \|_ /        we really want to compute
         * -y    p2
         */
        float edge1 = 2f * Mathf.Asin(0.5f * (p2Pos - p3Pos).magnitude);
        float edge2 = 2f * Mathf.Asin(0.5f * (p3Pos - p1Pos).magnitude);
        float edge3 = 2f * Mathf.Asin(0.5f * (p1Pos - p2Pos).magnitude);

        // measure of the angles at v in the triangle wedges spanned by v and the triangle p1-p2-p3
        float h = 0.5f * (edge1 + edge2 + edge3);

        if(Mathf.PI - h <= precision)
        {
            // handle special case where v is inside the triangle (on the same plane)
            // (then those three angles are almost a full circle <=> 2h is almost 2PI)

            // use only these barycentric coordinates as weights
            // weights from other triangles should be ignored
            weight1 = Mathf.Sin(edge1) * p2Dist * p3Dist;
            weight2 = Mathf.Sin(edge2) * p1Dist * p3Dist;
            weight3 = Mathf.Sin(edge3) * p1Dist * p2Dist;
            //gib breakpoint
            return true;
        }

        // calc inner angles of the triangle p1-p2-p3 with the half-angle formula 
        // with a_i being the angle at the point p_i

        // helper values:
        float sh = Mathf.Sin(h);
        float sinE1inv = 1f / Mathf.Sin(edge1);
        float sinE2inv = 1f / Mathf.Sin(edge2);
        float sinE3inv = 1f / Mathf.Sin(edge3);

        // compute the cosines of the inner angles
        float a1Cos = (2f * sh * Mathf.Sin(h - edge1) * sinE2inv * sinE3inv) - 1f;
        float a2Cos = (2f * sh * Mathf.Sin(h - edge2) * sinE1inv * sinE3inv) - 1f;
        float a3Cos = (2f * sh * Mathf.Sin(h - edge3) * sinE1inv * sinE2inv) - 1f;

        // compute the sines of the inner angles
        float a1Sin = Mathf.Sqrt(1 - Mathf.Pow(a1Cos, 2));
        float a2Sin = Mathf.Sqrt(1 - Mathf.Pow(a2Cos, 2));
        float a3Sin = Mathf.Sqrt(1 - Mathf.Pow(a3Cos, 2));

        // check if any sin should be negative with det[p1, p2, p3]
        // TODO: this doesn't seem to work
        /** 3x3 determinant
        * |a b c|
        * |d e f| => det = aei + bfg + cdh - (ceg + bdi + afh) 
        * |g h i|
        */
        float determinant = 
              p1Pos.x * p2Pos.y * p3Pos.z
            + p2Pos.x * p3Pos.y * p1Pos.z
            + p3Pos.x * p1Pos.y * p2Pos.z
            - p3Pos.x * p2Pos.y * p1Pos.z
            - p2Pos.x * p1Pos.y + p3Pos.z
            - p1Pos.x * p3Pos.y + p2Pos.z;

        if(a1Sin <= precision || a2Sin <= precision || a3Sin <= precision )
//            || Mathf.Sign(determinant) < 0)
        {
            // v is on the same plane, but outside of the current triangle
            // => ignore current triangle. return all weights as 0
            return false;
        }

        // calculate weights
        weight1 = (edge1 - a2Cos * edge3 - a3Cos * edge2) * sinE2inv / (2f * a3Sin * p1Dist);
        weight2 = (edge2 - a3Cos * edge1 - a1Cos * edge3) * sinE3inv / (2f * a1Sin * p2Dist);
        weight3 = (edge3 - a1Cos * edge2 - a2Cos * edge1) * sinE1inv / (2f * a2Sin * p3Dist);
        
        return false;
    }
    #endregion weight calculation

    #region Additional Stuff (unnecessary)

    // Triangle calculation Daniel
    private bool calculateTriangleWeightsDaniel(
    Vector3 p1Pos, Vector3 p2Pos, Vector3 p3Pos,
    float p1Dist, float p2Dist, float p3Dist,
    out float weight1, out float weight2, out float weight3)
    {
        weight1 = weight2 = weight3 = 0;

        float edge1 = 2f * Mathf.Asin(0.5f * (p2Pos - p3Pos).magnitude);
        float edge2 = 2f * Mathf.Asin(0.5f * (p3Pos - p1Pos).magnitude);
        float edge3 = 2f * Mathf.Asin(0.5f * (p1Pos - p2Pos).magnitude);

        float h = 0.5f * (edge1 + edge2 + edge3);

        if (Mathf.PI - h <= precision)
        {
            weight1 = Mathf.Sin(edge1) * p2Dist * p3Dist;
            weight2 = Mathf.Sin(edge2) * p1Dist * p3Dist;
            weight3 = Mathf.Sin(edge3) * p1Dist * p2Dist;
            return true;
        }

        // HERE STARTS DANIEL CODE
        // taken from https://github.com/julien-tierny/Scientific-Visualization/blob/master/mvcJacobianHessian/include/MVCoordinates3D.h computeCoordinatesOriginalCode

        float c1 = (2.0f * Mathf.Sin(h) * Mathf.Sin(h - edge1)) / (Mathf.Sin(edge2) * Mathf.Sin(edge3)) - 1.0f;
        float c2 = (2.0f * Mathf.Sin(h) * Mathf.Sin(h - edge2)) / (Mathf.Sin(edge3) * Mathf.Sin(edge1)) - 1.0f;
        float c3 = (2.0f * Mathf.Sin(h) * Mathf.Sin(h - edge3)) / (Mathf.Sin(edge1) * Mathf.Sin(edge2)) - 1.0f;

        float signBasis = 1.0f;
        if (Vector3.Dot(Vector3.Cross(p1Pos, p2Pos), p3Pos) < 0.0f)
            signBasis = -1.0f;

        float s1 = signBasis * Mathf.Sqrt(Mathf.Max(0.0f, 1.0f - c1 * c1));
        float s2 = signBasis * Mathf.Sqrt(Mathf.Max(0.0f, 1.0f - c2 * c2));
        float s3 = signBasis * Mathf.Sqrt(Mathf.Max(0.0f, 1.0f - c3 * c3));

        float w1 = (edge1 - c2 * edge3 - c3 * edge2) / (2.0f * p1Dist * Mathf.Sin(edge2)) * s3;
        float w2 = (edge2 - c3 * edge1 - c1 * edge3) / (2.0f * p2Dist * Mathf.Sin(edge3)) * s1;
        float w3 = (edge3 - c1 * edge2 - c2 * edge1) / (2.0f * p3Dist * Mathf.Sin(edge1)) * s2;

        weight1 = w1;
        weight2 = w2;
        weight3 = w3;

        return false;
    }

    // Linear weights calculation (buggy)
    private void calculateWeightsLinear()
    {
        int numOfMeshPoints = ownMeshRef.mesh.vertexCount;
        int numOfCagePoints = cageMeshRef.mesh.vertexCount;

        float[] totalWeights = new float[numOfMeshPoints];

        // array for saving weights
        float[,] weights = new float[numOfMeshPoints,numOfCagePoints];

        // get cagepoints in world space
        Vector3[] worldCage = new Vector3[numOfCagePoints];
        for (int i = 0; i < numOfCagePoints; i++)
        {
            worldCage[i] = cagePoints[i].position;
        }

        // for each mesh vertex:
        for (int vertexIndex = 0; vertexIndex < numOfMeshPoints; vertexIndex++)
        {
            // get vertex in world space
            Vector3 worldSelf = transform.TransformPoint(ownMeshRef.mesh.vertices[vertexIndex]);

            // normalize cage points
            float[] cagePointDistances = new float[numOfCagePoints];

            float totalDistance = 0f;
            for (int i = 0; i < numOfCagePoints; i++)
            {
                cagePointDistances[i] = (worldCage[i] - worldSelf).magnitude;
                totalDistance += cagePointDistances[i];
            }
            Debug.Log("TotalDistance " + totalDistance);

            // from this point on the current vertex is assumed to be at (0,0,0)
            // also there are no cage points directly at the current vertex (worldSelf)

            // calculate weights for every cagepoint
            for (int i = 0; i < numOfCagePoints; i++)
            {
                // add weight to point
                weights[vertexIndex,i] = 1.0f - (cagePointDistances[i] / totalDistance);
                Debug.Log(weights[vertexIndex,i]);
            }

        }

        cageWeights = weights;
    }
    #endregion

    #region mesh update

    public void updateMesh()
    {
        if (ComputeOnGPU)
            updateMeshGPU();
        else
            updateMeshCPU();

    }

    #region mesh deformation GPU

    private void setWeightsBuffer()
    {
        if (weightsBuffer != null)
            return;
        weightsBuffer = new ComputeBuffer(cageWeights.Length, sizeof(float));
        weightsBuffer.SetData(cageWeights);
        VertexUpdateShader.SetBuffer(kernelID, "weights", weightsBuffer);
    }

    void OnEnable()
    {
        // initially this is called before weights are set in Start()
        if (cageWeights != null)
            setWeightsBuffer();
    }

    void OnDisable()
    {
        if (weightsBuffer != null)
        {
            weightsBuffer.Release();
            weightsBuffer = null;
        }
    }

    private void updateMeshGPU()
    {
        // shader var numOfCagePoints is set once in Start()
        // shader var weights is first set in Start(),
        // then released in OnDisable() / reset in OnEnable()

        int numOfMeshVertices = ownMeshRef.mesh.vertexCount;
        int numOfCagePoints = cagePoints.Length;

        Transform ownTr = ownMeshRef.transform;
 
        // set cagePoints
        ComputeBuffer cagePointsBuffer = new ComputeBuffer(numOfCagePoints, 4 * sizeof(float));
        RawVec4[] bufferFillArray = new RawVec4[cagePoints.Length];
        for (int i = 0; i < numOfCagePoints; i++)
        {
            Vector3 pos = scaleMesh * ownTr.InverseTransformPoint(cagePoints[i].position);
            bufferFillArray[i].x = pos.x;
            bufferFillArray[i].y = pos.y;
            bufferFillArray[i].z = pos.z;
            bufferFillArray[i].w = 1f;
        }
        cagePointsBuffer.SetData(bufferFillArray);
        VertexUpdateShader.SetBuffer(kernelID, "cageBuffer", cagePointsBuffer);


        // set out mesh points
        RawVec4[] outMeshPoints = new RawVec4[numOfMeshVertices];
        ComputeBuffer outVertexBuffer = new ComputeBuffer(numOfMeshVertices, 4 * sizeof(float));
        outVertexBuffer.SetData(outMeshPoints);
        VertexUpdateShader.SetBuffer(kernelID, "meshBuffer", outVertexBuffer);

        VertexUpdateShader.Dispatch(kernelID, numOfMeshVertices, numOfCagePoints, 1);

        outVertexBuffer.GetData(outMeshPoints);

        Vector3[] meshVertices = new Vector3[numOfMeshVertices];
        for (int i = 0; i < numOfMeshVertices; i++)
        {
            meshVertices[i] = new Vector3(outMeshPoints[i].x, outMeshPoints[i].y, outMeshPoints[i].z);
        }

        cagePointsBuffer.Release();
        outVertexBuffer.Release();

        ownMeshRef.mesh.vertices = meshVertices;
    }
    #endregion mesh deformation GPU

    private void updateMeshCPU()
    {
        /* Updates Mesh vertices position. */
        Vector3[] vertices = ownMeshRef.mesh.vertices;

        //string debugOut = "";

        Vector3[] cpv = new Vector3[cagePoints.Length];
        for (int i = 0; i < cpv.Length; i++)
            cpv[i] = scaleMesh * ownMeshRef.transform.InverseTransformPoint(cagePoints[i].position);

        for (int currMeshVertex = 0; currMeshVertex < vertices.Length; currMeshVertex++)
        {
            Vector3 newPos = Vector3.zero;
            for (int currCageVertex = 0; currCageVertex < cagePoints.Length; currCageVertex++)
            {
                // sum weightened world positions of all cage points
                newPos += cageWeights[currMeshVertex,currCageVertex] * cpv[currCageVertex];
            }
            vertices[currMeshVertex] = newPos;

            //debugOut += vertices[currMeshVertex].ToString();
        }
        //Debug.Log(debugOut);
        ownMeshRef.mesh.vertices = vertices;
    }

    private void colorateMesh()
    {
        /* Colorates vertices according to the weight of the cageVertexTarget. Also colorates CagePoints. */
        Color colorSelected = Color.blue;
        Color colorUnselected = Color.white;

        // make target Cagepoint blue (rest white)
        for (int i = 0; i < cagePoints.Length; i++)
        {
            cagePoints[i].gameObject.GetComponent<Renderer>().material.color = colorUnselected;
        }
        cagePoints[cageVertexTarget].gameObject.GetComponent<Renderer>().material.color = colorSelected;

        Color[] colors = new Color[ownMeshRef.mesh.vertices.Length];

        // go over every vertex, get the cageweight for it and color it accordingly
        for (int currVertex = 0; currVertex < colors.Length; currVertex++)
        {
            // Debug.Log(cageWeights[currVertex][cageVertexTarget]);
            colors[currVertex] = Color.Lerp(colorUnselected, colorSelected, cageWeights[currVertex,cageVertexTarget] * colorIntensity);
        }

        ownMeshRef.mesh.colors = colors;
    }

    #endregion mesh update
}
